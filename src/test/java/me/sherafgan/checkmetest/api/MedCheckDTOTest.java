package me.sherafgan.checkmetest.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.dropwizard.jackson.Jackson;
import me.sherafgan.checkmetest.Util;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Map;

import static java.util.Map.entry;
import static org.assertj.core.api.Assertions.assertThat;

public class MedCheckDTOTest {
    private static final ObjectMapper MAPPER = Jackson.newObjectMapper();
    private MedCheckDTO medCheckDTO;

    @BeforeEach
    void setUp() {
        medCheckDTO = new MedCheckDTO(1, "General Check", Map.ofEntries(entry(3, 1500)));
    }

    @Test
    public void serializeToJSON() throws IOException, URISyntaxException {
        final String expected = MAPPER.writeValueAsString(
                MAPPER.readValue(Util.getResourceFile("fixtures/medCheckDTO.json"), MedCheckDTO.class));
        assertThat(MAPPER.writeValueAsString(medCheckDTO)).isEqualTo(expected);
    }

    @Test
    public void deserializeToJSON() throws IOException, URISyntaxException {
        assertThat(MAPPER.readValue(Util.getResourceFile("fixtures/medCheckDTO.json"), MedCheckDTO.class))
                .isEqualToComparingFieldByField(medCheckDTO);
    }
}
