package me.sherafgan.checkmetest.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.dropwizard.jackson.Jackson;
import me.sherafgan.checkmetest.Util;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Map;

import static java.util.Map.entry;
import static org.assertj.core.api.Assertions.assertThat;

public class ClinicDTOTest {
    private static final ObjectMapper MAPPER = Jackson.newObjectMapper();
    private ClinicDTO clinicDTO;

    @BeforeEach
    void setUp() {
        clinicDTO = new ClinicDTO(3, "Best Clinic", "Centralnaya, 1A", Map.ofEntries(entry(1, 1500)));
    }

    @Test
    public void serializeToJSON() throws IOException, URISyntaxException {
        final String expected = MAPPER.writeValueAsString(
                MAPPER.readValue(Util.getResourceFile("fixtures/clinicDTO.json"), ClinicDTO.class));
        assertThat(MAPPER.writeValueAsString(clinicDTO)).isEqualTo(expected);
    }

    @Test
    public void deserializeFromJSON() throws IOException, URISyntaxException {
        assertThat(MAPPER.readValue(Util.getResourceFile("fixtures/clinicDTO.json"), ClinicDTO.class))
                .isEqualToComparingFieldByField(clinicDTO);
    }
}
