package me.sherafgan.checkmetest;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;

public class Util {
    public static String getResourceFile(String fileRelativePath) throws IOException, URISyntaxException {
        File fixtureFile = new File("src/test/resources/" + fileRelativePath);
        String result = Files.readString(Path.of(fixtureFile.getAbsolutePath()));
        if (result != null && !result.isBlank()) {
            return result;
        }
        throw new FileNotFoundException("Couldn't find file in resources!");
    }
}
