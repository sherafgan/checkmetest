package me.sherafgan.checkmetest.resources;

import io.dropwizard.testing.junit5.DropwizardExtensionsSupport;
import io.dropwizard.testing.junit5.ResourceExtension;
import me.sherafgan.checkmetest.core.MedCheck;
import me.sherafgan.checkmetest.db.MedCheckDAO;
import org.junit.Ignore;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.mockito.Mockito.mock;

@ExtendWith(DropwizardExtensionsSupport.class)
public class MedCheckResourceTest {
    private static final MedCheckDAO DAO = mock(MedCheckDAO.class);
    private static final ResourceExtension EXT = ResourceExtension.builder()
            .addResource(new MedCheckResource(DAO))
            .build();
    private MedCheck medCheck;

    @BeforeEach
    void setup() {
        medCheck = new MedCheck("General Check");
        medCheck.setMedCheckId(1);
    }

    @Ignore
    @Test
    public void createMedCheck_ok() {
        //todo
    }

    @Ignore
    @Test
    public void getMedCheckWith() {
        //todo
    }

    @Ignore
    @Test
    public void getAllMedChecks_ok() {
        //todo
    }

    @Ignore
    @Test
    public void updateMedCheck_ok() {
        //todo
    }

    @Test
    public void deleteMedCheck_ok() {
        //todo
    }
}
