package me.sherafgan.checkmetest.resources;

import io.dropwizard.testing.junit5.DropwizardExtensionsSupport;
import io.dropwizard.testing.junit5.ResourceExtension;
import me.sherafgan.checkmetest.api.ClinicDTO;
import me.sherafgan.checkmetest.core.Clinic;
import me.sherafgan.checkmetest.db.ClinicDAO;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.ws.rs.client.Entity;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static java.util.Map.entry;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@ExtendWith(DropwizardExtensionsSupport.class)
public class ClinicResourceTest {
    private static final ClinicDAO DAO = mock(ClinicDAO.class);
    private static final ResourceExtension EXT = ResourceExtension.builder()
            .addResource(new ClinicResource(DAO))
            .build();
    private Clinic expectedClinic;

    @BeforeEach
    void setup() {
        expectedClinic = new Clinic("Best Clinic", "Centralnaya, 1A");
        expectedClinic.setClinicId(3);
    }

    @AfterEach
    void tearDown() {

    }

    @Test
    public void createClinic_ok() {
        int expectedId = 3;
        when(DAO.insertClinic(anyString(), anyString())).thenReturn(expectedId);
        ClinicDTO clinicDTO = new ClinicDTO("Best Clinic", "Centralnaya, 1A");
        int clinicId = EXT.target("/clinics").request().post(Entity.json(clinicDTO), Integer.class);
        assertEquals(expectedId, clinicId);
    }

    @Test
    public void createClinicWithMedCheckPrices_ok() {
        int expectedId = 3;
        when(DAO.insertClinicWithMedCheckPrices(anyString(), anyString(), anyMap())).thenReturn(expectedId);
        ClinicDTO clinicDTO = new ClinicDTO(null, "Best Clinic", "Centralnaya, 1A", Map.ofEntries(entry(1, 1500)));
        int clinicId = EXT.target("/clinics").request().post(Entity.json(clinicDTO), Integer.class);
        assertEquals(expectedId, clinicId);
    }

    @Test
    public void getClinic_ok() {
        when(DAO.getClinic(3)).thenReturn(expectedClinic);
        Clinic resultClinic = EXT.target("/clinics/3").request().get(Clinic.class);
        assertThat(expectedClinic).isEqualToComparingFieldByField(resultClinic);
    }

    @Test
    public void getAllClinics_ok() {
        List<Clinic> expectedClinics = Collections.singletonList(expectedClinic);
        when(DAO.getAllClinics()).thenReturn(expectedClinics);
        List<Clinic> resultClinics = EXT.target("/clinics").request().get(List.class);
        assertThat(expectedClinics.get(0)).isEqualToComparingFieldByField(resultClinics.get(0));

    }

    @Test
    public void updateClinic_ok() {
        ClinicDTO update = new ClinicDTO("Super Best Clinic", "Centralnaya, 2");
        update.setClinicId(3);
        doNothing().when(DAO).updateClinicNameAndAddress(anyInt(), anyString(), anyString());
        EXT.target("/clinics/3").request().put(Entity.json(update));
        verify(DAO).updateClinicNameAndAddress(anyInt(), anyString(), anyString());
        update = new ClinicDTO();
        update.setClinicId(3);
        update.setName("Super Best Clinic");
        doNothing().when(DAO).updateClinicName(anyInt(), anyString());
        EXT.target("/clinics/3").request().put(Entity.json(update));
        verify(DAO).updateClinicName(anyInt(), anyString());
        update = new ClinicDTO();
        update.setClinicId(3);
        update.setAddress("Centralnaya, 2");
        doNothing().when(DAO).updateClinicAddress(anyInt(), anyString());
        EXT.target("/clinics/3").request().put(Entity.json(update));
        verify(DAO).updateClinicAddress(anyInt(), anyString());
    }

    @Test
    public void addMedChecksTo_ok() {
        ClinicDTO dtoWithMedChecks =
                new ClinicDTO(3, "Best Clinic", "Centralnaya, 1A", Map.ofEntries(entry(2, 1600)));
        doNothing().when(DAO).insertMedChecksPriceInClinic(anyInt(), anyMap());
        EXT.target("/clinics/3/medChecks").request().put(Entity.json(dtoWithMedChecks));
        verify(DAO).insertMedChecksPriceInClinic(anyInt(), anyMap());
    }

    public void deleteMedChecksFrom_ok() {
        doNothing().when(DAO).deleteMedChecksFromClinic(anyInt(), anyList());
        EXT.target("/clinics/3/medChecks").request().delete();
        verify(DAO).deleteMedChecksFromClinic(anyInt(), anyList());
    }

    public void deleteClinicWith_ok() {
        doNothing().when(DAO).deleteClinic(anyInt());
        EXT.target("/clinics/3").request().delete();
        verify(DAO).deleteClinic(anyInt());
    }
}
