package me.sherafgan.checkmetest.db;

import me.sherafgan.checkmetest.core.MedCheck;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

public interface MedCheckDAO {
    @SqlUpdate("CREATE TABLE IF NOT EXISTS MedCheck (medCheckId INT NOT NULL AUTO_INCREMENT, name VARCHAR(255) NOT NULL, primary key(medCheckId))")
    void createMedCheckTable();

    @SqlUpdate("CREATE TABLE IF NOT EXISTS MedCheck_Clinic_Price (medCheckId INT NOT NULL, clinicId INT NOT NULL, price INT NOT NULL, primary key(medCheckId, clinicId), constraint fk_medCheckId foreign key (medCheckId) references MedCheck(medCheckId) on delete set null, constraint fk_clinicId foreign key (clinicId) references Clinic(clinicId) on delete set null)")
    void createMedCheckPriceInClinicTable();

    @Transaction
    default Integer insertMedCheckWithPrices(String name, Map<Integer, Integer> clinicIds2Prices) {
        int result = insertMedCheck(name);
        Iterator<Integer> clinicIds = clinicIds2Prices.keySet().iterator();
        while (clinicIds.hasNext()) {
            Integer clinicId = clinicIds.next();
            insertMedCheckPriceInClinic(result, clinicId, clinicIds2Prices.get(clinicId));
        }
        return result;
    }

    @SqlUpdate("INSERT INTO MedCheck (name) VALUES (:name)")
    @GetGeneratedKeys
    Integer insertMedCheck(@Bind("name") String name);

    @SqlUpdate("INSERT INTO MedCheck_Clinic_Price (medCheckId, clinicId, price) VALUES (:medCheckId, :clinicId, :price)")
    Integer insertMedCheckPriceInClinic(@Bind("medCheckId") Integer medCheckId,
                                        @Bind("clinicId") Integer clinicId, @Bind("price") Integer price);

    @RegisterBeanMapper(MedCheck.class)
    @SqlQuery("SELECT * FROM MedCheck WHERE medCheckId = :medCheckId")
    MedCheck getMedCheck(@Bind("medCheckId") Integer medCheckId);

    @RegisterBeanMapper(MedCheck.class)
    @SqlQuery("SELECT * FROM MedCheck")
    List<MedCheck> getAllMedChecks();

    @SqlUpdate("UPDATE MedCheck SET name = :name WHERE medCheckId = :medCheckId")
    void updateMedCheckName(@Bind("medCheckId") Integer medCheckId, @Bind("name") String name);

    @SqlUpdate("UPDATE MedCheck_Clinic_Price SET price = :price WHERE medCheckId = :medCheckId AND clinicId = :clinicId")
    void updateMedCheckPrice(@Bind("price") Integer price,
                             @Bind("medCheckId") Integer medCheckId, @Bind("clinicId") Integer clinicId);

    @Transaction
    default void updateMedCheckPrices(Integer medCheckId, Map<Integer, Integer> clinicIds2Prices) {
        Iterator<Integer> clinicIds = clinicIds2Prices.keySet().iterator();
        while (clinicIds.hasNext()) {
            Integer clinicId = clinicIds.next();
            updateMedCheckPrice(clinicIds2Prices.get(clinicId), medCheckId, clinicId);
        }
    }

    @Transaction
    default void updateNameAndMedCheckPrices(Integer medCheckId, String name, Map<Integer, Integer> clinicIds2Prices) {
        updateMedCheckName(medCheckId, name);
        updateMedCheckPrices(medCheckId, clinicIds2Prices);
    }

    @SqlUpdate("DELETE FROM MedCheck WHERE medCheckId = :medCheckId")
    void deleteMedCheck(@Bind("medCheckId") Integer medCheckId);
}
