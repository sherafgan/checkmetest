package me.sherafgan.checkmetest.db;

import me.sherafgan.checkmetest.core.Clinic;
import org.jdbi.v3.sqlobject.SingleValue;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

public interface ClinicDAO {
    @SqlUpdate("DROP ALL OBJECTS;")
    void dropAllObjects();

    @SqlUpdate("CREATE TABLE IF NOT EXISTS Clinic (clinicId INT NOT NULL AUTO_INCREMENT, name VARCHAR(255) NOT NULL, address VARCHAR(255) NOT NULL, primary key(clinicId))")
    void createClinicTable();

    @SqlUpdate("INSERT INTO Clinic (name, address) VALUES (:name, :address)")
    @GetGeneratedKeys
    Integer insertClinic(@Bind("name") String name, @Bind("address") String address);

    @Transaction
    default Integer insertClinicWithMedCheckPrices(String name, String address,
                                                   Map<Integer, Integer> medCheckIds2Prices) {
        int result = insertClinic(name, address);
        Iterator<Integer> medCheckIds = medCheckIds2Prices.keySet().iterator();
        while (medCheckIds.hasNext()) {
            int medCheckId = medCheckIds.next();
            insertMedCheckPriceInClinic(medCheckId, result, medCheckIds2Prices.get(medCheckId));
        }
        return result;
    }

    @SqlUpdate("INSERT INTO MedCheck_Clinic_Price (medCheckId, clinicId, price) VALUES (:medCheckId, :clinicId, :price)")
    Integer insertMedCheckPriceInClinic(@Bind("medCheckId") Integer medCheckId,
                                        @Bind("clinicId") Integer clinicId, @Bind("price") Integer price);

    @Transaction
    default void insertMedChecksPriceInClinic(Integer clinicId, Map<Integer, Integer> medCheckIds2Prices) {
        Iterator<Integer> medCheckIds = medCheckIds2Prices.keySet().iterator();
        while (medCheckIds.hasNext()) {
            Integer medCheckId = medCheckIds.next();
            insertMedCheckPriceInClinic(medCheckId, clinicId, medCheckIds2Prices.get(medCheckId));
        }
    }

    @RegisterBeanMapper(Clinic.class)
    @SqlQuery("SELECT * FROM Clinic WHERE clinicId = :clinicId")
    Clinic getClinic(@Bind("clinicId") Integer clinicId);

    @RegisterBeanMapper(Clinic.class)
    @SqlQuery("SELECT * FROM Clinic")
    List<Clinic> getAllClinics();

    @SqlUpdate("UPDATE Clinic SET name = :name WHERE clinicId = :clinicId")
    void updateClinicName(@Bind("clinicId") Integer clinicId, @Bind("name") String name);

    @SqlUpdate("UPDATE Clinic SET address = :address WHERE clinicId = :clinicId")
    void updateClinicAddress(@Bind("clinicId") Integer clinicId, @Bind("address") String address);

    @Transaction
    default void updateClinicNameAndAddress(Integer clinicId, String name, String address) {
        updateClinicName(clinicId, name);
        updateClinicAddress(clinicId, address);
    }

    @SqlUpdate("DELETE FROM MedCheck_Clinic_Price WHERE clinicId = :clinicId AND medCheckId = :medCheckId")
    void deleteMedCheckFromClinic(@Bind("clinicId") Integer clinicId, @Bind("medCheckId") Integer medCheckId);

    @Transaction
    default void deleteMedChecksFromClinic(Integer clinicId, List<Integer> medCheckIds) {
        for (int i = 0; i < medCheckIds.size(); i++) {
            deleteMedCheckFromClinic(clinicId, medCheckIds.get(i));
        }
    }

    @SqlUpdate("DELETE FROM Clinic WHERE clinicId = :clinicId")
    void deleteClinic(@Bind("clinicId") Integer clinicId);
}
