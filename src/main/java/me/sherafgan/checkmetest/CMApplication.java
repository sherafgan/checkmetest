package me.sherafgan.checkmetest;

import io.dropwizard.Application;
import io.dropwizard.jdbi3.JdbiFactory;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import me.sherafgan.checkmetest.db.ClinicDAO;
import me.sherafgan.checkmetest.db.MedCheckDAO;
import me.sherafgan.checkmetest.resources.ClinicResource;
import me.sherafgan.checkmetest.resources.MedCheckResource;
import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.core.h2.H2DatabasePlugin;
import org.jdbi.v3.sqlobject.SqlObjectPlugin;

import java.util.HashMap;
import java.util.Map;

public class CMApplication extends Application<CMConfiguration> {

    public static void main(final String[] args) throws Exception {
        new CMApplication().run(args);
    }

    @Override
    public String getName() {
        return "checkmetest";
    }

    @Override
    public void initialize(final Bootstrap<CMConfiguration> bootstrap) {
    }

    @Override
    public void run(final CMConfiguration configuration,
                    final Environment environment) {
        final JdbiFactory factory = new JdbiFactory();
        final Jdbi jdbi = factory.build(environment, configuration.getDataSourceFactory(), "h2");
        jdbi.installPlugin(new H2DatabasePlugin());
        jdbi.installPlugin(new SqlObjectPlugin());
        // BEGIN: create tables and add test data
        final ClinicDAO clinicDAO = jdbi.onDemand(ClinicDAO.class);
        clinicDAO.dropAllObjects();
        clinicDAO.createClinicTable();
        int openC = clinicDAO.insertClinic("Open Clinic", "Centralnaya, 1A");
        int bestC = clinicDAO.insertClinic("Best Clinic", "Severnaya, 13");
        int familyC = clinicDAO.insertClinic("Family Clinic", "Ohotnaya, 2/1");
        final MedCheckDAO medCheckDAO = jdbi.onDemand(MedCheckDAO.class);
        medCheckDAO.createMedCheckTable();
        medCheckDAO.createMedCheckPriceInClinicTable();
        Map<Integer, Integer> clinicIds2Prices = new HashMap<>();
        clinicIds2Prices.put(openC, 1200);
        clinicIds2Prices.put(bestC, 1800);
        clinicIds2Prices.put(familyC, 2000);
        medCheckDAO.insertMedCheckWithPrices("General check", clinicIds2Prices);
        // END
        environment.jersey().register(new ClinicResource(clinicDAO));
        environment.jersey().register(new MedCheckResource(medCheckDAO));
//        environment.jersey().register(new JdbiExceptionsBundle());
    }

}
