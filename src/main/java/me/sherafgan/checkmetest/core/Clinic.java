package me.sherafgan.checkmetest.core;

import com.fasterxml.jackson.annotation.JsonInclude;

import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Clinic {
    private Integer clinicId;
    @NotEmpty
    private String name;
    @NotEmpty
    private String address;
    private List<MedCheck> medChecks;

    public Clinic() {
    }

    public Clinic(String name, String address) {
        this.name = name;
        this.address = address;
        medChecks = new ArrayList<>();
    }

    public Integer getClinicId() {
        return clinicId;
    }

    public void setClinicId(Integer clinicId) {
        this.clinicId = clinicId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<MedCheck> getMedChecks() {
        return medChecks;
    }

    public void setMedChecks(List<MedCheck> medChecks) {
        this.medChecks = medChecks;
    }
}
