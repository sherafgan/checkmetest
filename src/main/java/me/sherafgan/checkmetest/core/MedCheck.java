package me.sherafgan.checkmetest.core;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MedCheck {
    private Integer medCheckId;
    private String name;
    private List<Clinic> clinics;
    /**
     * Minimum (default) price of the medical check.
     */
    private Integer price;

    public MedCheck() {
    }

    public MedCheck(String name) {
        this.name = name;
        this.clinics = new ArrayList<>();
    }

    public Integer getMedCheckId() {
        return medCheckId;
    }

    public void setMedCheckId(Integer medCheckId) {
        this.medCheckId = medCheckId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Clinic> getClinics() {
        return clinics;
    }

    public void setClinics(List<Clinic> clinics) {
        this.clinics = clinics;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }
}
