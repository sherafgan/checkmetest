package me.sherafgan.checkmetest.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ClinicDTO {
    @JsonProperty
    private Integer clinicId;
    @JsonProperty
    private String name;
    @JsonProperty
    private String address;
    @JsonProperty
    private Map<Integer, Integer> medCheckIds2Prices;

    public ClinicDTO(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public ClinicDTO() {
    }

    public ClinicDTO(Integer clinicId, String name, String address, Map<Integer, Integer> medCheckIds2Prices) {
        this.clinicId = clinicId;
        this.name = name;
        this.address = address;
        this.medCheckIds2Prices = medCheckIds2Prices;
    }

    public Integer getClinicId() {
        return clinicId;
    }

    public void setClinicId(Integer clinicId) {
        this.clinicId = clinicId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Map<Integer, Integer> getMedCheckIds2Prices() {
        return medCheckIds2Prices;
    }

    public void setMedCheckIds2Prices(Map<Integer, Integer> medCheckIds2Prices) {
        this.medCheckIds2Prices = medCheckIds2Prices;
    }
}
