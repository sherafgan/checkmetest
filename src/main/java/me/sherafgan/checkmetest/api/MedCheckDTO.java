package me.sherafgan.checkmetest.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MedCheckDTO {
    @JsonProperty
    private Integer medCheckId;
    @JsonProperty
    private String name;
    @JsonProperty
    private Map<Integer, Integer> clinicIds2Prices;

    public MedCheckDTO() {
    }

    public MedCheckDTO(Integer medCheckId, String name, Map<Integer, Integer> clinicIds2Prices) {
        this.medCheckId = medCheckId;
        this.name = name;
        this.clinicIds2Prices = clinicIds2Prices;
    }

    public Integer getMedCheckId() {
        return medCheckId;
    }

    public void setMedCheckId(Integer medCheckId) {
        this.medCheckId = medCheckId;
    }

    public Map<Integer, Integer> getClinicIds2Prices() {
        return clinicIds2Prices;
    }

    public void setClinicIds2Prices(Map<Integer, Integer> clinicIds2Prices) {
        this.clinicIds2Prices = clinicIds2Prices;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
