package me.sherafgan.checkmetest.resources;

import me.sherafgan.checkmetest.api.ClinicDTO;
import me.sherafgan.checkmetest.core.Clinic;
import me.sherafgan.checkmetest.db.ClinicDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.Map;

@Path("/clinics")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ClinicResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(ClinicResource.class);
    private final ClinicDAO clinicDAO;

    public ClinicResource(ClinicDAO clinicDAO) {
        this.clinicDAO = clinicDAO;
    }

    @POST
    public Integer createClinic(@NotNull @Valid ClinicDTO clinicDTO) {
        Map<Integer, Integer> medChecksIds2Prices = clinicDTO.getMedCheckIds2Prices();
        if (medChecksIds2Prices == null || medChecksIds2Prices.isEmpty()) {
            return clinicDAO.insertClinic(clinicDTO.getName(), clinicDTO.getAddress());
        }
        return clinicDAO.insertClinicWithMedCheckPrices(clinicDTO.getName(), clinicDTO.getAddress(), medChecksIds2Prices);
    }

    @GET
    @Path("/{clinicId}")
    public Clinic getClinicWith(@NotNull @PathParam("clinicId") Integer clinicId) {
        return clinicDAO.getClinic(clinicId);
    }

    @GET
    public List<Clinic> getAllClinics() {
        return clinicDAO.getAllClinics();
    }

    @PUT
    @Path("/{clinicId}")
    public void updateClinic(@NotNull @PathParam("clinicId") Integer clinicId,
                             @Valid @NotNull ClinicDTO clinicDTO) {
        if (clinicDTO.getName() != null && clinicDTO.getAddress() != null) {
            clinicDAO.updateClinicNameAndAddress(clinicDTO.getClinicId(), clinicDTO.getName(), clinicDTO.getAddress());
        } else {
            if (clinicDTO.getName() != null) {
                clinicDAO.updateClinicName(clinicDTO.getClinicId(), clinicDTO.getName());
            }
            if (clinicDTO.getAddress() != null) {
                clinicDAO.updateClinicAddress(clinicDTO.getClinicId(), clinicDTO.getAddress());
            }
        }
    }

    @PUT
    @Path("/{clinicId}/medChecks")
    public void addMedChecksTo(@NotNull @PathParam("clinicId") Integer clinicId, @NotNull ClinicDTO clinicDTO) throws Exception {
        if (clinicDTO.getMedCheckIds2Prices() != null && !clinicDTO.getMedCheckIds2Prices().isEmpty()) {
            clinicDAO.insertMedChecksPriceInClinic(clinicId, clinicDTO.getMedCheckIds2Prices());
        }
        throw new Exception("'medCheckIds2Prices' is not present or empty!");
    }

    @DELETE
    @Path("/{clinicId}/medChecks")
    public void deleteMedChecksFrom(@NotNull @PathParam("clinicId") Integer clinicId, @NotEmpty List<Integer> medCheckIds) {
        clinicDAO.deleteMedChecksFromClinic(clinicId, medCheckIds);
    }

    @DELETE
    @Path("/{clinicId}")
    public void deleteClinicWith(@NotNull @PathParam("clinicId") Integer clinicId) {
        clinicDAO.deleteClinic(clinicId);
    }
}
