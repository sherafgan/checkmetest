package me.sherafgan.checkmetest.resources;

import me.sherafgan.checkmetest.api.MedCheckDTO;
import me.sherafgan.checkmetest.core.MedCheck;
import me.sherafgan.checkmetest.db.MedCheckDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.Map;

@Path("/medChecks")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class MedCheckResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(MedCheckResource.class);
    private final MedCheckDAO medCheckDAO;

    public MedCheckResource(MedCheckDAO medCheckDAO) {
        this.medCheckDAO = medCheckDAO;
    }

    @POST
    public Integer createMedCheck(MedCheckDTO medCheckDTO) throws Exception {
        String name = medCheckDTO.getName();
        if (name == null || name.trim().isEmpty()) {
            throw new Exception("At least 'name' of the Medical Check is needed to create it!");
        }
        Map<Integer, Integer> clinicIds2Prices = medCheckDTO.getClinicIds2Prices();
        if (clinicIds2Prices != null && !clinicIds2Prices.isEmpty()) {
            return medCheckDAO.insertMedCheckWithPrices(name, clinicIds2Prices);
        }
        return medCheckDAO.insertMedCheck(medCheckDTO.getName());
    }

    @GET
    @Path("/{medCheckId}")
    public MedCheck getMedCheckWith(@NotNull @PathParam("medCheckId") Integer medCheckId) {
        return medCheckDAO.getMedCheck(medCheckId);
    }

    @GET
    public List<MedCheck> getAllMedChecks() {
        return medCheckDAO.getAllMedChecks();
    }

    @PUT
    @Path("/{medCheckId}")
    public void updateMedCheck(@NotNull @PathParam("medCheckId") Integer medCheckId,
                               @NotNull MedCheckDTO medCheckDTO) throws Exception {
        String name = medCheckDTO.getName();
        boolean updateName = name != null && !name.isEmpty();
        Map<Integer, Integer> clinicIds2Prices = medCheckDTO.getClinicIds2Prices();
        boolean updatePrices = clinicIds2Prices != null && !clinicIds2Prices.isEmpty();
        if (updatePrices && updateName) {
            medCheckDAO.updateNameAndMedCheckPrices(medCheckId, name, clinicIds2Prices);
        } else if (updatePrices) {
            medCheckDAO.updateMedCheckPrices(medCheckId, clinicIds2Prices);
        } else if (updatePrices) {
            medCheckDAO.updateMedCheckName(medCheckId, medCheckDTO.getName());
        } else {
            throw new Exception("Nothing to update!");
        }
    }

    @DELETE
    @Path("/{medCheckId}")
    public void deleteMedCheck(@NotNull @PathParam("medCheckId") Integer medCheckId) {
        medCheckDAO.deleteMedCheck(medCheckId);
    }
}
